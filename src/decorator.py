#!/usr/bin/env python

from functools import wraps

def spam( repeats ):
	def decorator( func ):
		@wraps( func )
		def wrapper( *args, **kwargs ):
			for i in range( repeats ):
				func( *args, **kwargs )
		return wrapper
	return decorator

def decorator( func ):
	@wraps( func )
	def wrapper( arg ):
		return func( '<i>'+arg+'</i>' )
	return wrapper

@spam( 10 )
@decorator
def greet( message ):
	print message

greet( 'hello, world!' )

class Decorator(object):
    def __init__(self, f):
        self.f = f
        wraps(f)(self)
    def __call__(self, *args, **kwargs):
        return self.f( *args, **kwargs)

@Decorator
def foo(): 
	return 'bar'

print foo()
