#!/usr/bin/env python

import types

class Publisher( object ):
	def __init__( self, channel ):
		self.channel = channel

	def publish( self, channel, *args, **argv ):
		self.channel.publish( channel, *args, **argv )

class Subscriber( object ):
	def __init__( self, f, channel, filter = None ):
		self.f = f
		self.channel = channel
		if filter:
			self.filter = types.MethodType( filter, self, *args, **kwargs )

	def filter( self, *args, **kwagrs ):
		pass

	def notify( self, *args, **kwargs ):
		self.filter( *args, **kwargs )
		self.f( *args, **kwargs )

	def subscribe( self, channel ):
		self.channel.subscribe( channel, self )

	def unsubscribe( self, channel ):
		self.channel.unsubscribe( channel, self )

class Channel( object ):
	def __init__( self ):
		self.channels = {}
		self.channels['broadcast'] = [];

	def publish( self, channel, *args, **kwargs ):
		if( channel in self.channels.keys() and channel != 'broadcast' ):
			for subscriber in self.channels[ channel ]:
				subscriber.notify( channel, *args, **kwargs )
		for subscriber in self.channels[ 'broadcast' ]:
			subscriber.notify( channel, *args, **kwargs )

	def subscribe( self, channel, subscriber ):
		if( channel not in self.channels.keys() ):
			self.channels[ channel ] = []
		self.channels[ channel ].append( subscriber )

	def remove( self, subscriber ):
		for channel in self.channels.itervalues():
			if( subscriber in channel ):
				channel.remove( subscriber )
				if( len( channel ) == 0 and channel != 'broadcast' ):
					del self.channels[ channel ]

	def unsubscribe( self, channel, subscriber ):
		if( channel in self.channels.keys() 
				and subscriber in self.channels[ channel ] ):
			self.channels[ channel ].remove( subscriber )
			if( len( self.channels[ channel ] ) == 0 
					and channel != 'broadcast' ):
				del self.channels[ channel ]

if __name__ == "__main__":
	channel = Channel()
	p = Publisher( channel )
	def handler( channel, msg ):
		print channel, msg
	s1 = Subscriber( handler, channel )
	s1.subscribe( '1' )
	s2 = Subscriber( handler, channel )
	s2.subscribe( '2' )
	p.publish( '1', 'Message1' )
	p.publish( '2', 'Message2' )
