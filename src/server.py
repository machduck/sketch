#!/usr/bin/env python

import zerorpc
import gevent

q = gevent.queue.Queue()

class StreamingRPC(object):
	@zerorpc.stream
	def streaming_range(self, fr, to, step):
		for msg in q:
			yield( msg )

s = zerorpc.Server(StreamingRPC())
s.bind("tcp://0.0.0.0:4242")
e = gevent.spawn(s.run)

for i in range(5):
	q.put( i )
	gevent.sleep( i )

e.join()
