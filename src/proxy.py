#!/usr/bin/env python

class Proxy( object ):
	def __init__( self, subject ):
		object.__setattr__( self, 'inner', subject )
	def __getattr__( self, attr ):
		return getattr( self.inner, attr )
	def __setattr__( self, value ):
		setattr( self.inner, attr, value )

if __name__ == "__main__":

	class Foo( object ):
		def foo( self, bar ):
			return bar

	w = Proxy( Foo() )
	print w.foo( 'bar' )
