#!/usr/bin/env python

class MetaClass( type ):
	def __new__( cls, name, bases, attrs ):
		print '[Metaclass]'
		return type.__new__( cls, name, bases, attrs )

def ClassDecorator( cls ):
	print '[Decorator]'
	return cls

@ClassDecorator
class Class( object ):
	__metaclass__ = MetaClass
	pass

class ManglingType( type ):
	def __new__( cls, name, bases, attrs ):
		for attr, value in attrs.items():
			if attr.startswith("__"):
				continue
			attrs[ "foo_" + attr ] = value
			del attrs[attr]
		return type.__new__( cls, name, bases, attrs )

class MangledClass:
	__metaclass__ = ManglingType

	def hi( self ):
		print "hi"

	def bye( self ):
		print "bye"

if __name__ == "__main__":

	m = MangledClass()
	m.foo_hi()
	m.foo_bye()

	c = Class()
