#!/usr/bin/env python

from functools import wraps

def fsm( cls ):
	cls._classes = {}
	@classmethod
	def _register( statecls ):
		cls._classes[ statecls.__name__ ] = statecls
	cls._register = _register
	return cls

def state( f ):
	@wraps( f )
	def decorator( self, *args, **kwargs ):
		result = f( self, *args, **kwargs )
		self.next()
		return result
	return decorator

def next( *transitions ):
	def decorator( cls ):
		cls._transitions = transitions
		cls._register()
		return cls
	return decorator

class Fsm( object ):
	def switch( self, cls = None ):
		if( cls is None ):
			self.next()
		else:
			self.__class__ = self._classes[ cls ]
	def next( self ):
		for state in self._transitions:
			statecls = self._classes[ state ]
			result = statecls.transition( self.__class__, self )
			if( result ):
				self.__del__()
				self.__class__ = statecls
				self.__init__()
				return True
		return False
	def __del__( self ):
		pass

if __name__ == "__main__":
	@fsm
	class Protocol( Fsm ):
		def status( self ):
			return '[STATUS] %s' % self._status
		@classmethod
		def transition( this, cls, self ):
			return True

	@next( 'Stage2' )
	class Stage1( Protocol ):
		_status = 'STAGE-1'
		@state
		def state( self ): pass

	@next()
	class Stage2( Protocol ):
		_status = 'STAGE-2'

	@fsm
	class OtherProtocol( Fsm ):
		def status( self ):
			return '[STATUS] %s' % self._status
		@classmethod
		def transition( this, cls, self ):
			return True
		
	@next( 'Stage4' )
	class Stage3( OtherProtocol ):
		_status = 'STAGE-3'
		@state
		def state( self ): pass

	@next()
	class Stage4( OtherProtocol ):
		_status = 'STAGE-4'

	fsm = Stage1()
	print fsm.status()
	fsm.state()
	print fsm.status()

	fsm = Stage3()
	print fsm.status()
	fsm.state()
	print fsm.status()
