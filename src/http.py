#!/usr/bin/python

from twisted.internet import defer, reactor
from twisted.names import client
from twisted.internet.protocol import Protocol
from twisted.internet.protocol import ReconnectingClientFactory as ClientFactory

from twisted.python import log
import sys

class HTTPClientBase( Protocol ):
	def __init__( self, deferred ):
		self.data = ''
		self.deferred = deferred
	def dataReceived( self, data ):
		log.msg( 'Received %d data', len(data) )
		self.data += data
	def connectionMade( self ):
		log.msg( 'Protocol connected' )
		self.request()
	def connectionLost( self, reason ):
		log.msg( 'Protocol connection lost' )
		self.finished()
	def request( self ):
		raise NotImplementedError, 'Overload this method, client initiates'
	def finished( self ):
		self.deferred.callback( self.data )

class HTTPClient( HTTPClientBase ):
	def __init__( self, *args, **kwargs ):
		HTTPClientBase.__init__( self, *args, **kwargs )
	def request( self ):
		log.msg( 'Protcol attempting communication' )
		self.transport.write( 'GET / HTTP/1.0\r\n' )
		self.transport.write( 'Host: google.ru\r\n' )
		self.transport.write( '\r\n' )
		log.msg( 'Protocol communication finished' )

class HTTPClientFactoryBase( ClientFactory ):
	def __init__( self ):
		self.deferred = defer.Deferred()
	def startedConnecting( self, connector ):
		log.msg( 'Initiating connection' )
	def buildProtocol( self, addr ):
		log.msg( 'Factory connected', addr )
		log.msg( 'Preparing to build client' )
		client = HTTPClient( self.deferred )
		log.msg( 'Client is built' )
		client.deferred = self.deferred
		log.msg( 'Defer is injected' )
		return client
	def clientConnectionLost( self, connector, reason ):
		log.msg( 'Factory connection lost:', reason )
	def clientConnectionFailed( self, connector, reason ):
		log.msg( 'Factory connection failed:', reason )

class HTTPClientFactory( HTTPClientFactoryBase ):
	def __init__( self, *args, **kwargs ):
		HTTPClientFactoryBase.__init__( self, *args, **kwargs )
	def getDeferred( self ):
		return self.deferred

class Handler:
	def __init__( self, logging = True ):
		if( logging ): log.startLogging( sys.stdout )
	def open( self, ip, port, host, path ):
		factory = HTTPClientFactory()
		reactor.connectTCP( ip, port, factory )
		return factory.getDeferred()

def printData( data ):
	print data

def finish( ignore ):
	print '[*] Done'
	reactor.stop()

def main():
	handler = Handler()
	d1 = handler.open( '173.194.32.151', 80, 'google.ru', '/' )
	d2 = handler.open( '173.194.32.152', 80, 'google.ru', '/' )
	d3 = handler.open( '173.194.32.159', 80, 'google.ru', '/' )
	d1.addBoth( printData )
	d2.addBoth( printData )
	d3.addBoth( printData )
	d = defer.DeferredList( [d1, d2, d3], consumeErrors = True )
	d.addCallback( finish )
	reactor.run()

if __name__ == '__main__':
	main()
