#!/usr/bin/env python

class DictToObject( object ):
	def __init__( self, **kwargs ):
		self.__dict__.update( kwargs )

from threading import Lock

def syn( lock = Lock() ):
	def decorator( f ):
		@wraps( f )
		def wrapper( self, *args, **kwargs ):
			with lock:
				return f( self, *args, **kwargs )
		return wrapper
	return decorator

class Emitter( object ):
	def __init__( self ):
		self.actions = {};
		self._default = lambda *args, **kwargs: None

	def on( self, action, cb ):
		self.actions[ action ] = cb
		return self

	def off( self, action ):
		del self.actions[ action ]
		return self

	def emit( self, action, *args, **kwargs ):
		if( action not in self.actions ):
			return self._default( *args, **kwargs )
		else:
			return self.actions[ action ]( *args, **kwargs )

	def events( self ):
		return self.actions.keys()

	def default( self, cb ):
		self._default = cb
		return self

class StaticEmitter( Emitter ):
	_actions = {}

	@classmethod
	def on( cls, action, cb ):
		cls._actions[ action ] = cb

	def __init__( self ):
		Emitter.__init__( self )
		self.actions.update( self._actions )

def on( emitter, trigger ):
	def decorator( f ):
		emitter.on( trigger, f )
		@wraps( f )
		def wrapper( *args, **kwargs ):
			return f( *args, **kwargs )
		return wrapper
	return decorator

class Subject( object ):
	def __init__( self ):
		self.observers = [];

	def bind( self, observer ):
		self.observers.append( observer )

	def notify( self, *arg, **kwargs ):
		for observer in self.observers:
			observer.event( *arg, **kwargs )

class Observer( object ):
	def __init__( self, cb ):
		self.handler = cb

	def event( self, *arg, **kwargs ):
		self.handler( *arg, **kwargs )

	def observe( self, subject ):
		subject.bind( self )
