#!/usr/bin/env python

from nose import with_setup
from nose.tools import assert_equals

import path

class Namespace( object ): pass

ns = Namespace()

def setup_emitter():
	pass
def teardown_emitter():
	global ns
	del ns

@with_setup( setup_emitter, teardown_emitter )
def test_demo():
	assert_equals( "demo", "demo" )

class TestDemo( object ):
	def setup( self ):
		self.ns = Namespace()
	def teardown( self ):
		del self.ns
	@classmethod
	def setup_class( cls ):
		pass
	@classmethod
	def teardown_class( cls ):
		pass
	def test_demo( self ):
		assert_equals( 1, 1 )
		#assert_equals( "demo", "fail" )
