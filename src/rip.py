#!/usr/bin/env python

import gevent
from gevent.monkey import patch_all
patch_all()

from gevent.queue import JoinableQueue as Queue

import dns.reversename
import dns.resolver
import dns.name

import iptools

def worker( q ):
	resolver = dns.resolver.Resolver()
	while True:
		ip = q.get()
		rip = dns.reversename.from_address(ip)
		names = resolver.query( rip, 'PTR' )
		for host in names:
			name = dns.name.from_text( str(host) )
			level = len(name.labels)
			for i in range( 0, level-2 ):
				url = ".".join( name.labels[i:] )[:-1]
				print '%s => %s | level => %d' % ( ip, url, level-i-1 )
		q.task_done()

q = Queue()
workers = []
for i in range( 10 ):
	workers.append( gevent.spawn( worker, q ) )

targets = iptools.IpRangeList(
		( '8.8.4.4', '8.8.4.4' )
		)

for target in targets:
	q.put( target )

q.join()
